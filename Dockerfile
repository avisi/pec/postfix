FROM registry.gitlab.com/avisi/base/centos:7

EXPOSE 25

ARG MAILSPOOL="/var/spool/postfix"
ARG POSTFIX_PACKAGE_VERSION=3.4.0-1.gf.el7

ENV MAILSPOOL=${MAILSPOOL} \
    MYNETWORKS="10.0.0.0/8,127.0.0.0/8,172.17.0.0/16"

VOLUME $MAILSPOOL

USER root

RUN rpm -ivh http://mirror.ghettoforge.org/distributions/gf/gf-release-latest.gf.el7.noarch.rpm \
    && yum update -y \
    && yum-config-manager --enable gf-testing \
    && yum install -y postfix3-${POSTFIX_PACKAGE_VERSION} telnet cyrus-sasl cyrus-sasl-lib cyrus-sasl-plain \
    && yum clean all

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT [ "/entrypoint.sh" ]
