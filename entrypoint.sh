#!/bin/sh

set -e

SMTP_RELAY_HOST=${SMTP_RELAY_HOST?Missing env var SMTP_RELAY_HOST}
SMTP_RELAY_USERNAME=${SMTP_RELAY_USERNAME}
SMTP_RELAY_PASSWORD=${SMTP_RELAY_PASSWORD}

postconf 'inet_protocols=ipv4'

# handle sasl
if [ ! -z "$SMTP_RELAY_USERNAME" ] && [ ! -z "$SMTP_RELAY_PASSWORD" ]; then
    echo -n "${SMTP_RELAY_HOST} ${SMTP_RELAY_USERNAME}:${SMTP_RELAY_PASSWORD}" > /etc/postfix/sasl_passwd
    postmap /etc/postfix/sasl_passwd

    postconf 'smtp_sasl_auth_enable = yes'
    postconf 'smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd'
    postconf 'smtp_sasl_security_options = noanonymous'

fi

postconf 'smtp_use_tls = yes'
# where to find CA certificates
#smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt

# These are required.
postconf "relayhost = ${SMTP_RELAY_HOST}"
postconf "myhostname = $(hostname)"

# Override what you want here. The 10. network is for kubernetes
postconf "mynetworks = ${MYNETWORKS}"

postconf "inet_interfaces = all"

# http://www.postfix.org/COMPATIBILITY_README.html#smtputf8_enable
postconf 'smtputf8_enable = no'

# This makes sure the message id is set. If this is set to no dkim=fail will happen.
postconf 'always_add_missing_headers = yes'

exec postfix start-fg
