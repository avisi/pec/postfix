# Docker Postfix

This repository contains an Postfix installation based on a CentOS base image.

[![pipeline status](https://gitlab.com/avisi/pec/postfix/badges/master/pipeline.svg)](https://gitlab.com/avisi/pec/postfix/commits/master)

---

### Tags

| Tag | Notes |
|-----|-------|
| `3.3.2` | Postfix v3.3.2 |
| `latest` | Latest build of v3.3.2 |

## Usage

Example docker-compose file:
```yml
version: '3'
services:
  postfix:
    image: registry.gitlab.com/avisi/pec/postfix:3.3.2
    ports: ['25:25']
    environment:
      SMTP_RELAY_USERNAME: username
      SMTP_RELAY_PASSWORD: changeit
      SMTP_RELAY_HOST: relayhost:587

```

## Issues

All issues should be reported in the [Gitlab issue tracker](https://gitlab.com/avisi/pec/postfix/issues).
